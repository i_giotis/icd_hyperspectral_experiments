# to activate conda environment
# ..\..\AppData\Local\Continuum\Anaconda3\Scripts\activate.bat

import pickle
import json
import getpass
import pyodbc
import pandas
import numpy as np
from joblib import Parallel, delayed


def connect_to_database(server, database, username, password):
    cnxn = pyodbc.connect(r'DRIVER={ODBC Driver 11 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+
                          username+';PWD='+ password+';Trusted_Connection=yes')
    return cnxn


def get_identical_pills(data, unique_drugs, i, thr):
    pairs = []
    x = data.loc[data['drug'] == unique_drugs[i]][data.columns[-5:]]
    drug_i = x.loc[~(x == 0).all(axis=1)]
    for j in range(i + 1, len(unique_drugs)):
        y = data.loc[data['drug'] == unique_drugs[j]][data.columns[-5:]]
        drug_j = y.loc[~(y == 0).all(axis=1)]
        if match_list_instances(drug_i, drug_j, thr):
            pairs.append((unique_drugs[i], unique_drugs[j]))
            print('matched:', (unique_drugs[i], unique_drugs[j]))
    print('Finished drug: ', i)
    return pairs


def match_list_instances(df_i, df_j, thr):
    for _, inst_i in df_i.iterrows():
        for _, inst_j in df_j.iterrows():
            dist = np.abs(inst_i - inst_j) / inst_i
            if max(dist) <= thr:
                return True
    return False


if __name__ == '__main__':

    # load file of pill sale percentages
    f = open('pill_sales.p3', 'rb')
    pill_sales = pickle.load(f)
    f.close()

    # database parameters
    server = 'PC_IOANNIS\SQLEXPRESS'
    database = 'dispensercheck'
    username = 'i.giotis'
    password = getpass.getpass('Password:')

    # create connection object
    cnx = connect_to_database(server, database, username, password)

    # select from reference drugs query
    main_query = "SELECT ref_id, drug, Red, Green, Blue, Ell_Length, Ell_Width FROM b_ref_drug"

    #top_drugs_query = "SELECT DISTINCT * FROM (SELECT TOP 48 drug from b_batch_bag_drug GROUP BY drug " \
    #                  "ORDER BY COUNT(0) desc) a"

    # execute query with pandas to get DataFrame
    data = pandas.read_sql(main_query, cnx)
    unique_drugs = pill_sales['drug'].values

    thr = 0.05
    result = Parallel(n_jobs=-1)(delayed(get_identical_pills)(data, unique_drugs, i, thr)
                                 for i in range(1, len(unique_drugs)))
    flattened = [val for sublist in result for val in sublist]

    final = []
    for elem in flattened:
        tmp = dict(zip(elem, [pill_sales.loc[pill_sales['drug'] == elem[0]]['tab_count'].values[0],
                         pill_sales.loc[pill_sales['drug'] == elem[1]]['tab_count'].values[0]]))
        final.append(tmp)
    f = open('top48_pairs_5.json', 'w')
    json.dump(final, f)
    f.close()
