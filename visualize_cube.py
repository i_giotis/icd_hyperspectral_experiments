# Interactive plot of series of hyperspectral images
# Cmd line argument: path to numpy file that contains the data cube
#import matplotlib
#matplotlib.use('TkAgg')

import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

global view
global fig
global cube

cube = np.load(sys.argv[1]+'test_cube.npy')

fig, ax = plt.subplots()
plt.subplots_adjust(left=0.25, bottom=0.25)
f0 = 400
view = ax.imshow(cube[f0, :, :], cmap='hot')

axcolor = 'lightgoldenrodyellow'
axwl = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=axcolor)
slider = Slider(axwl, 'Wavelength', 0, 2047, valinit=f0, valfmt='%d')

def update(val):
    view.set_data(cube[int(val), :, :])
    #ax.imshow(cube[int(val), :, :], 'gray')
    fig.canvas.draw()
    #plt.draw()



def arrow_key_image_control(event):
    """
    This function takes an event from an mpl_connection
    and listens for key release events specifically from
    the keyboard arrow keys (left/right) and uses this
    input to advance/reverse to the next/previous image.
    """
    minindex = slider.valmin
    maxindex = slider.valmax
    if event.key == 'left':
        if slider.val - 1 >= minindex:
            slider.set_val(slider.val-1)
            update(slider.val)
        else:
            pass
    elif event.key == 'right':
        if slider.val + 1 <= maxindex:
            slider.set_val(slider.val + 1)
            update(slider.val)
        else:
            pass
    else:
        pass

slider.on_changed(update)
id2 = fig.canvas.mpl_connect('key_release_event', arrow_key_image_control)
plt.show()
plt.pause(0.0001)