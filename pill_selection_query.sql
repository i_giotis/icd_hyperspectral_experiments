
with selection as
(select distinct * from
 (select top 48 bbd.drug from b_batch_bag_drug bbd
  group by bbd.drug
  order by count(0) desc) a
 union
 select '00093947'
 union
 select '00115878'
 union
 select '00115983'
 union
 select '15579018'
 union
 select '99999988')
select selection.drug, count(0) as bag_count, sum(tab_count) as tab_count
from selection inner join b_drug d on selection.drug = d.drug
     left join b_batch_bag_drug bbd on selection.drug = bbd.drug
where d.drug_name not like '%zepam%'
group by selection.drug

--select d.drug, count(0) as bag_count, isnull(sum(tab_count), 0) as tab_count
--from b_drug d
--     left join r_batch_bag_drug bbd on d.drug = bbd.drug
--group by d.drug


--select d.drug, d.drug_name, d.manufacturer, d.knmp_code, count(0), sum(tab_count)
--select sum(tab_count) from b_batch_bag_drug