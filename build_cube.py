import sys
import glob
import math
import numpy as np
from joblib import Parallel, delayed

# path with bin file as command line argument
# the data cube is:
# X -> wavelengths
# Y -> locations
# Z -> time
# slice on the X-axis, i.e. cube[i,:,:]
# to get a the 2D image on a certain wavelength

def read_raw_file(file):
    raw = np.fromfile(file, dtype='int8', sep="")
    size = int(math.sqrt(raw.shape[0]))
    slice = raw.reshape([size, size])
    return slice

if __name__ == '__main__':
    path = sys.argv[1]+"*.bin"
    raw_files = glob.glob(path)
    slices = Parallel(n_jobs=-1)(delayed(read_raw_file)(file) for file in raw_files)
    cube = np.stack(slices, axis=2)

    np.save(sys.argv[1]+'test_cube.npy', cube)
