import getpass
import pyodbc
import pandas


def connect_to_database(server, database, username, password):
    cnxn = pyodbc.connect(r'DRIVER={ODBC Driver 11 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+
                          username+';PWD='+ password+';Trusted_Connection=yes')
    return cnxn


if __name__ == '__main__':
    # database parameters
    server = 'PC_IOANNIS\SQLEXPRESS'
    database = 'dispensercheck'
    username = 'i.giotis'
    password = getpass.getpass('Password:')

    # create connection object
    cnx = connect_to_database(server, database, username, password)

    f = open('pill_selection_query.sql', 'r')
    sql = f.read()
    f.close()

    # absolute numbers
    pill_sales = pandas.read_sql(sql, cnx)
    # percentage within 42-day period
    pill_sales.loc[:, 'bag_count':'tab_count'] = pill_sales.loc[:, 'bag_count':'tab_count'].\
        div(pill_sales.sum(axis=0).loc['bag_count':'tab_count'])

    pill_sales.to_pickle('pill_sales.p3')

