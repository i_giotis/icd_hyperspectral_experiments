# E:\hyperspectral_pills\Brennenstuhl_150\00016810_37.7_26.4_60d\test_cube.npy

import os
import cv2
import numpy as np
from matplotlib import pyplot as plt


def update_image(val):
    global cube, name
    cv2.imshow(name, cube[int(val), :, :])


def click_and_crop(event, x, y, flags, params):
    # grab references to the global variables
    global refPt, cropping, name, cube
    val = cv2.getTrackbarPos('Wavelength', name)
    clone = cube[int(val), :, :].copy()
    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being
    # performed
    if event == cv2.EVENT_LBUTTONDOWN:
        refPt = [(x, y)]
        cropping = True

    # check to see if the left mouse button was released
    elif event == cv2.EVENT_LBUTTONUP:
        # record the ending (x, y) coordinates and indicate that
        # the cropping operation is finished
        refPt.append((x, y))
        cropping = False

        # draw a rectangle around the region of interest
        cv2.rectangle(cube[int(val), :, :], refPt[0], refPt[1], (0, 255, 0), 2)
        cv2.imshow(name, cube[int(val), :, :])

        # if there are two reference points, then crop the region of interest
        # from teh image and display it
    #if len(refPt) == 2:
        roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
        #cv2.namedWindow("ROI", cv2.WINDOW_GUI_NORMAL)
        #cv2.resizeWindow("ROI", roi.shape[1], roi.shape[0])
        #cv2.imshow("ROI", roi)
        fig2 = plt.figure()
        ax21 = fig2.add_subplot(1, 2, 1)
        ax21.imshow(roi)
        #ax21.title("ROI")

        img2hist = cube[:, refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
        hist = cv2.calcHist(np.sum(img2hist, axis=0).astype(np.uint8), [0], None, [256], [0, 256])
        cv2.normalize(hist, hist, 0, 255, cv2.NORM_MINMAX)
        ax22 = fig2.add_subplot(1, 2, 2)
        ax22.plot(hist)
        ax22.xlim([0, 256])
        #ax22.title("Agg. Histogram")

        #cv2.waitKey(0) & 0xFF


#if __name__ == '__main__':
# argv[1] = path to data., argv[2] = size of visualization window
#name = sys.argv[1]
#win_size = int(sys.argv[2])

win_size = int(input("Enter the visualization window size: "))
name = input("Enter the path to the data cube: ")
print("Loading", os.path.getsize(name) >> 20, "MB...")
cube = np.load(name)
s0 = 0

# initialize the list of reference points and boolean indicating
# whether cropping is being performed or not
refPt = []
cropping = False

# Create window
cv2.namedWindow(name, cv2.WINDOW_GUI_NORMAL)
cv2.resizeWindow(name, win_size, win_size)
cv2.setMouseCallback(name, click_and_crop)
cv2.createTrackbar("Wavelength", name, s0, cube.shape[0], update_image)
cv2.waitKey(0) & 0xFF


