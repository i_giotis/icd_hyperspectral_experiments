import sys
from cx_Freeze import setup, Executable

# <added>
import os.path
PYTHON_INSTALL_DIR = os.path.dirname(os.path.dirname(os.__file__))
os.environ['TCL_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tcl8.6')
os.environ['TK_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tk8.6')
# </added>

base = 'Console'
#if sys.platform == 'win32':
#    base = 'Win32GUI'

executables = [
    Executable('opencv_hyperviz.py', base=base)
]

# <added>
options = {
    'build_exe': {
        'packages': ["cv2", "os", "numpy", "matplotlib"],
        'include_files': [
            os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tk86t.dll'),
            os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tcl86t.dll'),
         ],
    },
}
# </added>

setup(name='viz',
      version='0.1',
      description='Test cx_Freeze Viz',
      options=options,
      executables=executables
      )
